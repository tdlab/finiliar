// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.type == "status") sendResponse({status: localStorage.status});
    console.log(localStorage.status);
});
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.type == "wordCount") sendResponse({wordCount: localStorage.wordCount});
    console.log("background");
    var words = localStorage.getItem('wordCount')
    console.log(words);
});
